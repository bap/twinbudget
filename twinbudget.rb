require 'sinatra'

get '/' do
  erb :index
end

get '/demo' do
  @sheet_key = "0AsX-UMI0leX0dEc4UlpjV1Bqd3RENEZMSVVSNmxDYkE"
  erb :budget
end

get '/account' do
  erb :budget
end

not_found do
  erb :"404"
end
