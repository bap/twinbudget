/**
 * @fileoverview Functions handling Google Drive authentification
                 and formating spreadsheet data.
 * @author Baptiste Marchand <baptistemarchand42@gmail.com>
 */

/** @const */ var CLIENT_ID = '559673728219';
/** @const */ var API_KEY = 'AIzaSyC63stoekx6aw_Zt1C61OJFADIZGrWMsHQ';
/** @const */ var SCOPES = 'https://spreadsheets.google.com/feeds ' +
                           'https://www.googleapis.com/auth/drive';
/** @const */ var DEMO = false;
/** @const */ var SPREADSHEET_NAME = 'TwinBudget';

/**
 * Show some log info to the user
 * @param {string} The message to display.
 */
function log(text) {
    $("#log").text(text);
}

/**
 * Shows overview/history buttons and hide authorize button.
 * @param {boolean} enable If false then the function is reversed.
 */
function showMenu(enable) {
    log('');
    if (enable) {
        $('#history-button').css('display', 'inline-block');
        $('#overview-button').css('display', 'inline-block');
        $('#overview-button-last-month').css('display', 'inline-block');
        $('#authorize-button').css('display', 'none');
    }
    else {
        $('#history-button').css('display', 'none');
        $('#overview-button').css('display', 'none');
        $('#overview-button-last-month').css('display', 'none');
        $('#authorize-button').css('display', 'inline-block');
    }
}

/**
 * Logs in to Google Drive if necessary (not in Demo mode).
 */
function handleClientLoad() {
    if (KEY != '')
    {
        DEMO = true;
        showMenu(true);
        return;
    }
    log('Logging to Google Drive...');
    gapi.client.setApiKey(API_KEY);
    window.setTimeout(function() {
        gapi.auth.authorize({client_id: CLIENT_ID,
                             scope: SCOPES,
                             immediate: true},
                            handleAuthResult);
    }, 1);
}

/**
 * Checks the result of the authentification and shows the
 * "authorize" button if not connected.
 * @param authResult The result of the authentification process.
 */
function handleAuthResult(authResult) {
    if (authResult && !authResult.error) {
        getUserKey(function()
                   {
                       if (KEY == '')
                       {
                           $('#authorize-button').css('display', 'none');
                           log('TwinBudget spreadsheet not found!'
                                         + ' Make sure your spreadsheet is called "'
                                         + SPREADSHEET_NAME + '"');
                       }
                       else
                           showMenu(true);
                   });
    }
    else {
        showMenu(false);
        $('#authorize-button').click(function() {
            gapi.auth.authorize({client_id: CLIENT_ID,
                                 scope: SCOPES,
                                 immediate: false}, handleAuthResult);});
    }
}

/**
 * Tries to find the TwinBudget spreadsheet on the user's google drive and
 * sets KEY to the document's key
 * @param {function} complete The fonction that is called once we found the key.
 */
function getUserKey(complete) {
    log('Looking for your spreadsheet...');
    var url = 'https://spreadsheets.google.com/feeds/spreadsheets/private/full?alt=json-in-script&callback=findSpreadsheet&access_token=' + gapi.auth.getToken().access_token;
    $.getScript(url, complete).fail(function() {
        log('Could not access your spreadsheets on Google Drive.');
    });
}

function findSpreadsheet(json)
{
    $('#loginInfo').text(json.feed.title.$t.substring('vailable Spreadsheets - '.length));
    for (var i = 0; i < json.feed.entry.length ; i++) {
        if (json.feed.entry[i].title.$t == SPREADSHEET_NAME)
        {
            var href = json.feed.entry[i].link[1].href;
            KEY =  href.substr(href.indexOf('key=')+4);
        }
    }
}

/**
 * Loads a worksheet and launches a function to handle the data
 * @param {string} worksheet The ID of the worksheet we want to load (e.g: od6)
 * @param {function} callback The function that will parse the JSON we receive.
 */
function loadWorksheet(worksheet, callback) {
    document.getElementById('transactions').innerHTML = '';

    if (DEMO) {
        log('Loading demo spreadsheet...');
        var url = 'http://spreadsheets.google.com/feeds/list/'
            + KEY + '/' + worksheet
            + '/public/values?alt=json-in-script&callback=' + callback;
    }
    else {
        log('Loading your spreadsheet...');
        var url = 'https://spreadsheets.google.com/feeds/list/'
            + KEY + '/' + worksheet
            + '/private/full?alt=json-in-script&callback=' + callback
            + '&access_token=' + gapi.auth.getToken().access_token;
    }
    $.getScript(url).fail(function() {$('#log').text('Your spreadsheet is broken!')});
}

/**
 * Load the history worksheet
*/
function loadHistory() {
    loadWorksheet('odv', 'processHistory');
}

/**
 * Load the overview worksheet
 */
function loadOverview() {
    loadWorksheet('oci', 'processOverview');
}

/**
 * Load the last month overview worksheet
 */
function loadOverviewLastMonth() {
    loadWorksheet('ocn', 'processOverview');
}

var colorTable = {
	Autre:   '#051922',
	Boulot:  '#051932',
	Retrait: '#051942',
	Salaire: '#051952',

	Appart:      '#2e4b21',
	Assurance:   '#2e4b01',
	Electricite: '#2e4b11',
	Internet:    '#2e4b21',
	Loyer:       '#2e4b31',

	TaxesImpotsBanque: '#bac43c',
	ImpotsTaxes: '#bac43c',
	Banque:      '#bac44c',

	LoisirShopping: '#ba1f25',
	Cadeaux:  '#ba1f05',
	Habits:   '#ba1f15',
	Sorties:  '#ba1f25',
	HighTech: '#ba1f35',

	TousLesJours: '#70b5bf',
	Courses:   '#70b5af',
	Telephone: '#70b5bf',
	Transport: '#70b5cf'
};

/**
 * Gets a color based on the category's name.
 * If it's the first time we see this category we create a new random color.
 * @param {string} name The name of the category we want to color.
 */
function getColor(name) {
    if (!colorTable[name])
    {
        var color = '0' + Math.floor(Math.random()*16777215).toString(16)
        colorTable[name] = '#' + color.substr(color.length-6);
    }
    return colorTable[name];
}

/**
 * Adds a new element to the entry described by "li".
 * @param li List element that we want to populate.
 * @param {string} name The type of field we want to add.
 * @param {string} data The data we want to add.
 * @param entry The entry from which the data comes
 */
function addField(li, name, data, entry)
{
    var field = document.createElement('div');
    // Cells sometimes contain a leading single quote so we have to delete it.
    if (name == 'montant') {
    	var prefix = '';
    	if (entry.gsx$out.$t.replace("'", '') == 'Moi' && entry.gsx$in.$t.replace("'", '') != 'Moi')
    		prefix = '-';
    	if (entry.gsx$out.$t.replace("'", '') != 'Moi' && entry.gsx$in.$t.replace("'", '') == 'Moi')
    		prefix = '+';
        field.appendChild(document.createTextNode(prefix + data.replace("'", '')
                                                  + entry.gsx$devise.$t.replace("'", '')));
    }
    else if (name == 'photo') {
    	var a = document.createElement('a');
    	a.setAttribute('href', data);
    	var img = document.createElement('img');
    	img.setAttribute('alt', 'Receipt picture');
    	img.setAttribute('src', 'img/receipt.png');
    	a.appendChild(img);
    	a.setAttribute('target', '_blank')
		field.appendChild(a);
    }
    else if (name == 'client') {
    	data = '';

    	if (entry.gsx$out.$t.replace("'", '') == 'Moi')
    		data += 'Moi/' + entry.gsx$moiout.$t.replace("'", '');
    	else if (entry.gsx$out.$t.replace("'", '') == 'Autre')
    		data += entry.gsx$autreclientout.$t.replace("'", '');
    	else
    		data += entry.gsx$out.$t.replace("'", '');

    	data += ' -> ';

		if (entry.gsx$in.$t.replace("'", '') == 'Moi')
    		data += 'Moi/' + entry.gsx$moiin.$t.replace("'", '');
    	else if (entry.gsx$in.$t.replace("'", '') == 'Autre')
    		data += entry.gsx$autreclientin.$t.replace("'", '');
    	else
    		data += entry.gsx$in.$t.replace("'", '');

        field.appendChild(document.createTextNode(data));
    }
    else
        field.appendChild(document.createTextNode(data.replace("'", '')));

    if (name == 'categorie') {
        field.style.background = getColor(data.replace("'", ''));
        field.setAttribute('class', name);
    }
    else if (name == 'pourcentage') {
        field.style.background = getColor(entry.gsx$e.$t.replace("'", ''));
        field.setAttribute('class', name + ' bar_' + entry.gsx$e.$t.replace("'", ''));
    }
    else
        field.setAttribute('class', name);
    li.appendChild(field);

}

/**
 * Creates a new list element for the History view.
 * @param entry The entry from the JSON feed containing the data
 */
function createHistoryEntry(entry) {
    var li = document.createElement('li');

    addField(li, 'categorie', entry.gsx$categorie.$t, entry);
    addField(li, 'montant', entry.gsx$montant.$t, entry);
    addField(li, 'client', '', entry);
    addField(li, 'note', entry.gsx$note.$t, entry);
    if (entry.gsx$photo.$t)
    	addField(li, 'photo', entry.gsx$photo.$t, entry);

    return li;
}

/**
 * Creates a new list element for the Overview.
 * @param entry The entry from the JSON feed containing the data
 */
function createOverviewEntry(entry) {
    var li = document.createElement('li');

    addField(li, 'categorie', entry.gsx$e.$t, entry);
    addField(li, 'pourcentage', parseFloat(entry.gsx$g.$t).toFixed(1) + '%', entry);
    addField(li, 'total', entry.gsx$f.$t + '€', entry);

    $('#transactions').append(li);
    $('.bar_'+entry.gsx$e.$t).animate(
        {'width': '+=' + parseFloat(entry.gsx$g.$t).toFixed()*5}, 300);
}

/**
 * Returns a well formated date from a Date object.
 * @param {Date} d The date we want to format.
 */
function formatDate(d)
{
    var d_names = new Array('Sunday', 'Monday', 'Tuesday',
                            'Wednesday', 'Thursday', 'Friday', 'Saturday');

    var m_names = new Array('January', 'February', 'March',
                            'April', 'May', 'June', 'July', 'August', 'September',
                            'October', 'November', 'December');

    var day = d.getDay();
    var date = d.getDate();
    var sup = 'th';
    if (date == 1 || date == 21 || date ==31)
        sup = 'st';
    else if (date == 2 || date == 22)
        sup = 'nd';
    else if (date == 3 || date == 23)
        sup = 'rd';
    var month = d.getMonth();
    var year = d.getFullYear();

    var today = new Date();
    var curr_year = today.getFullYear();

    return d_names[day] + ' ' + date + '<sup>'
        + sup + '</sup> ' + m_names[month] + ' ' + (year != curr_year ? year : '');
}

/**
 * Handles data from a history worksheet feed
 * @param json JSON feed returned by the server.
 */
function processHistory(json) {
    var transactions = $('#transactions');
    var len = json.feed.entry.length;
    if (json.feed.entry.length == 0)
        return;
    var previousDate = json.feed.entry[len - 1].gsx$date.$t;
    var a = json.feed.entry[len - 1].gsx$date.$t.split('/');
    var d = new Date(a[2], a[0]-1, a[1]);
    transactions.append('<hr /><div class="date">'+formatDate(d)+'</p>');
    for (var i = len - 1; i >= 0 ; i--) {
        if (previousDate != json.feed.entry[i].gsx$date.$t)
        {
            a = json.feed.entry[i].gsx$date.$t.split('/');
            d = new Date(a[2], a[0]-1, a[1]);
            transactions.append($('<hr /><div class="date">'+formatDate(d)+'</p>'));
        }
        previousDate = json.feed.entry[i].gsx$date.$t;
        transactions.append(createHistoryEntry(json.feed.entry[i]));
    }
    log('');
}

/**
 * Adds two entries to the transactions list with IN and OUT values.
 * @param {number} IN The total of all positive transactions.
 * @param {number} OUT The total of all negative transactions.
 */
function createSummary(IN, OUT)
{
    $('#transactions').append($('<li><div id="IN">+' + IN + '€</div></li>'));
    $('#transactions').append($('<li><div id="OUT">-' + OUT + '€</div></li>'));
    $('#IN').animate(
        {width: '+=' + IN/10}, 300);
    $('#OUT').animate(
        {width: '+=' + OUT/10}, 300);
}

/**
 * Handles data from an overview worksheet feed
 * @param json JSON feed returned by the server.
 */
function processOverview(json) {
    // Summary
    $('#transactions').append($('<li>Summary</li>'));
    createSummary(json.feed.entry[13].gsx$d.$t,
                  json.feed.entry[13].gsx$e.$t);

    // Expenses
    $('#transactions').append($('<li>Expenses</li>'));
    for (var i = 16; json.feed.entry[i].gsx$e.$t ; i++) {
        if (parseInt(json.feed.entry[i].gsx$f.$t) != 0)
            createOverviewEntry(json.feed.entry[i]);
    }
    // Groupes
    $('#transactions').append($('<li>Groupes</li>'));
    for (var i = 40; json.feed.entry[i].gsx$e.$t ; i++) {
        if (parseInt(json.feed.entry[i].gsx$f.$t) != 0)
            createOverviewEntry(json.feed.entry[i]);
    }
    log('');
}
